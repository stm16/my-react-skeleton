<?php

/* header with all scripts */

function head($props) {
    ?>
<html>
<head>
    <title><?=$props['title']?></title>
    <link rel="stylesheet" type="text/css" href="css/mui.min.css">
    <script type="javascript" src="js/react.min.js"></script>
    <script type="javascript" src="js/mui.min.js"></script>
    <script type="javascript" src="js/underscore-min.js"></script>
</head>

<?php
}

echo head(array("title"=>"My Sample Page"));

?>